import ez_setup
ez_setup.use_setuptools()

from setuptools import setup, find_packages

setup(
    name="salesforce-bulk-api-wrapper",
    version="0.0.3",
    packages=find_packages(),

    # Dependencies
    install_requires=[
        'beautifulsoup4==4.9.3',
        'lxml==4.6.3',
        'requests==2.25.1'
    ],

    # Metadata
    author='Will Clardy',
    author_email='wclardy@rev-a-shelf.com',
    description='A friendly wrapper around the Salesforce Bulk API',
    license='MIT',
    keywords='salesforce bulk api wrapper',
    url='https://gitlab.com/rev-a-shelf/salesforce-bulk-api-wrapper'
)
