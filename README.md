# Salesforce Bulk API Wrapper

This library provides a friendly Python wrapper around the Salesforce Bulk
API. It is far from complete, and is only expanded when Rev-A-Shelf has
need. Even so, it is quite usable, and provides a nice abstraction around
Salesforce's somewhat cumbersome API.

## Installation

The package is not, and probably never will be, on PyPI. To install, clone
this repo and run the following command from the repo's root directory.

```
$ pip install .
```

## Usage

The library exposes four classes (`Session`, `Job`, `Batch`, and `Result`)
which can be used to interact with the Salesforce Bulk API. Following is a
brief example of their use.

```python
import os
import time

from sf_bulk_api.session import Session
from sf_bulk_api.job import Job
from sf_bulk_api.batch import Batch
from sf_bulk_api.result import Result

username = os.environ['SALESFORCE_USERNAME']
password = os.environ['SALESFORCE_PASSWORD']
session = Session(username, password)

# Check https://goo.gl/vif9PC for possible job options.
# Currently, JSON is the only supported content type.
options = {
    'operation': 'query',
    'object': 'Account',
    'concurrencyMode': 'Parallel',
    'contentType': 'JSON'
}

# Using 'with' ensures that the job gets closed
with Job(session, options) as job:
    query = 'SELECT Id, Name FROM Account LIMIT 10'
    batch = Batch(session, job, query)

    # Check to see if the batch process is complete
    batch_info = batch.get_info()
    while batch_info['state'] != 'Completed':
        if batch_info['state'] == 'Failed':
            raise RuntimeError('Batch Failed', batch_info['stateMessage'])
        elif batch_info['state'] == 'Not Processed':
            raise RuntimeError('Batch Aborted', batch_info['stateMessage'])
        else:
            # Retry after 10 seconds
            time.sleep(10)
            batch_info = batch.get_info()

    result = Result(session, job, batch)
    while result.results:
        output = result.get(result.results.pop())
        # Do something with the output...
```
