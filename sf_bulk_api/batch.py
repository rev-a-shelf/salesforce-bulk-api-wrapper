import requests


class Batch:
    def __init__(self, session, job, soql):
        self.session_id = session.id
        self.job_id = job.id
        self.__host = (f'https://{session.instance}.my.salesforce.com'
                       '/services/async/39.0')
        self.__headers = {"X-SFDC-Session": session.id}
        self.__create(soql)

    def __create(self, soql):
        url = f'{self.__host}/job/{self.job_id}/batch'
        headers = {"Content-Type": "application/json; charset=UTF-8"}
        headers = {**self.__headers, **headers}
        response = requests.post(url, headers=headers, data=soql)

        if response.status_code >= 200 and response.status_code < 300:
            self.id = response.json()['id']

    def get_info(self, field=None):
        url = f'{self.__host}/job/{self.job_id}/batch/{self.id}'
        response = requests.get(url, headers=self.__headers)

        if response.status_code >= 200 and response.status_code < 300:
            output = response.json()

            if field is None:
                return output
            else:
                return output[field]
