import requests


class Result:
    def __init__(self, session, job, batch):
        self.session_id = session.id
        self.job_id = job.id
        self.batch_id = batch.id
        self.__host = (f'https://{session.instance}.my.salesforce.com'
                       '/services/async/39.0')
        self.__headers = {"X-SFDC-Session": session.id}
        self.__create()

    def __create(self):
        url = f'{self.__host}/job/{self.job_id}/batch/{self.batch_id}/result'
        response = requests.get(url, headers=self.__headers)

        if response.status_code >= 200 and response.status_code < 300:
            self.results = response.json()

    def get(self, result_id):
        url = (f'{self.__host}/job/{self.job_id}/batch/{self.batch_id}'
               f'/result/{result_id}')
        response = requests.get(url, headers=self.__headers)

        if response.status_code >= 200 and response.status_code < 300:
            return response.json()
