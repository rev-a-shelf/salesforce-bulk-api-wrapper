import re
from string import Template

from bs4 import BeautifulSoup
import requests

LOGIN_XML = """\
<?xml version="1.0" encoding="utf-8" ?>
<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
  <env:Body>
    <n1:login xmlns:n1="urn:partner.soap.sforce.com">
      <n1:username>$username</n1:username>
      <n1:password>$password</n1:password>
    </n1:login>
  </env:Body>
</env:Envelope>
"""


class Session:
    def __init__(self, username, password):
        self.jobs = {}
        self.batches = {}
        self.__login(username, password)

    def __login(self, username, password):
        url = "https://login.salesforce.com/services/Soap/u/53.0"
        headers = {
            "Content-Type": "text/xml; charset=UTF-8",
            "SOAPAction": "login"
        }
        body = Template(LOGIN_XML).substitute({
            'username': username,
            'password': password
        })
        response = requests.post(url, headers=headers, data=body)

        if response.status_code == 200:
            xml = BeautifulSoup(response.content, "xml")
            match = re.search("^http[s]?:\/\/([^.]+).*$", xml.serverUrl.string)
            self.id = xml.sessionId.string
            self.instance = match.group(1)
