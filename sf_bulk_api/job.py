import requests


class Job:
    def __init__(self, session, options):
        self.session_id = session.id
        self.__host = (f'https://{session.instance}.my.salesforce.com'
                       '/services/async/39.0')
        self.__headers = {"X-SFDC-Session": session.id}
        self.__create(options)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def __create(self, options):
        url = f'{self.__host}/job'
        headers = {"Content-Type": "application/json; charset=UTF-8"}
        headers = {**self.__headers, **headers}
        response = requests.post(url, headers=headers, json=options)

        if response.status_code >= 200 and response.status_code < 300:
            self.id = response.json()['id']

    def get_info(self, field=None):
        url = f'{self.__host}/job/{self.id}'
        response = requests.get(url, headers=self.__headers)

        if response.status_code >= 200 and response.status_code < 300:
            output = response.json()

            if field is None:
                return output
            else:
                return output[field]

    def close(self):
        url = f'{self.__host}/job/{self.id}'
        headers = {"Content-Type": "application/json; charset=UTF-8"}
        headers = {**self.__headers, **headers}
        body = {"state": "Closed"}
        requests.post(url, headers=headers, json=body)
